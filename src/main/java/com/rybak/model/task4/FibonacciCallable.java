package com.rybak.model.task4;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

public class FibonacciCallable implements Callable<Integer> {

    private static Logger log = LogManager.getLogger(FibonacciCallable.class);
    private int number;
    private List<Integer> sequence;

    public FibonacciCallable(int number) {
        this.number = number;
        sequence = new ArrayList<>();
    }

    private int getFibonacciNumber(int n) {
        if (n <= 1) {
            return n;
        }
        return getFibonacciNumber(n - 1) + getFibonacciNumber(n - 2);
    }

    private int getSumOfFibonacciSequence(int n) {
        int sum = 0;
        for (int i = 0; i < n; i++) {
            int fn = getFibonacciNumber(i);
            sum += fn;
        }
        return sum;
    }

    @Override
    public Integer call() throws Exception {
        return getSumOfFibonacciSequence(number);
    }

    @Override
    public String toString() {
        return "FibonacciCallable{" +
                "number=" + number +
                ", sequence=" + sequence +
                '}';
    }
}
