package com.rybak.model.task7_1;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

public class Receiver implements Runnable {

    PipedInputStream pipedInputStream;

    public Receiver(PipedOutputStream pipedOutputStream) {
        try {
            pipedInputStream = new PipedInputStream(pipedOutputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        int data = 0;
        try {
            data = pipedInputStream.read();
        } catch (IOException e) {
            e.printStackTrace();
        }
        while (data != -1) {
            try {
                data = pipedInputStream.read();
                System.out.print((char) data);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            pipedInputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}


