package com.rybak.model.task7_1;

import java.io.IOException;
import java.io.PipedOutputStream;

public class Sender implements Runnable {

    PipedOutputStream pipedOutputStream;

    public Sender() {
        pipedOutputStream = new PipedOutputStream();
    }

    @Override
    public void run() {
        try {
            pipedOutputStream.write("Info from sender".getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                pipedOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    public PipedOutputStream getPipedOutputStream() {
        return pipedOutputStream;
    }
}


