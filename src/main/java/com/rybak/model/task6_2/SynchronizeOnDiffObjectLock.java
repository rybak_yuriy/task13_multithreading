package com.rybak.model.task6_2;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class SynchronizeOnDiffObjectLock {


    private final static Lock firstLock = new ReentrantLock();
    private final static Lock secondLock = new ReentrantLock();
    private final static Lock thirdLock = new ReentrantLock();
    private static Logger log = LogManager.getLogger(SynchronizeOnDiffObjectLock.class);

    public void runFirstMethod() {
        firstLock.lock();
        try {
            for (int i = 0; i < 10; i++) {
                log.info("SynchronizeOnDiffObjectLock.Message form first method");
            }
        } finally {
            firstLock.unlock();
        }

    }

    public void runSecondMethod() {
        secondLock.lock();
        try {
            for (int i = 0; i < 10; i++) {
                log.info("SynchronizeOnDiffObjectLock.Message form second method");
            }
        } finally {
            secondLock.unlock();
        }

    }

    public void runThirdMethod() {
        thirdLock.lock();
        try {
            for (int i = 0; i < 10; i++) {
                log.info("SynchronizeOnDiffObjectLock.Message form third method");
            }
        } finally {
            thirdLock.unlock();
        }

    }
}
