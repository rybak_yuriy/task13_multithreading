package com.rybak.model.task6_2;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class SynchronizeOnSameObjectLock {

    private final static Lock lock = new ReentrantLock();
    private static Logger log = LogManager.getLogger(SynchronizeOnSameObjectLock.class);

    public void runFirstMethod() {
        lock.lock();
        try {
            for (int i = 0; i < 10; i++) {
                log.info("SynchronizeOnSameObjectLock.Message form first method");
            }
        } finally {
            lock.unlock();
        }

    }

    public void runSecondMethod() {
        lock.lock();
        try {
            for (int i = 0; i < 10; i++) {
                log.info("SynchronizeOnSameObjectLock.Message form second method");
            }
        } finally {
            lock.unlock();
        }

    }

    public void runThirdMethod() {
        lock.lock();
        try {
            for (int i = 0; i < 10; i++) {
                log.info("SynchronizeOnSameObjectLock.Message form third method");
            }
        } finally {
            lock.unlock();
        }

    }

}
