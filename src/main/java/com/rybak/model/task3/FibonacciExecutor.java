package com.rybak.model.task3;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;

public class FibonacciExecutor implements Runnable {
    private static Logger log = LogManager.getLogger(FibonacciExecutor.class);
    private int number;
    private int[] sequence;

    public FibonacciExecutor(int number) {
        this.number = number;
        sequence = new int[number];
    }

    private int getFibonacciNumber(int n) {
        if (n <= 1) {
            return n;
        }
        return getFibonacciNumber(n - 1) + getFibonacciNumber(n - 2);
    }

    @Override
    public void run() {
        for (int i = 0; i < number; i++) {
            sequence[i] = getFibonacciNumber(i);
        }
        log.info(Arrays.toString(sequence));
    }

    public int[] getSequence() {
        return sequence;
    }

}
