package com.rybak.model.task5;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

public class FifthTask implements Callable<Integer> {

    private static Logger log = LogManager.getLogger(FifthTask.class);
    Random random = new Random();
    private int sleepTime;

    public FifthTask() {
        sleepTime = random.nextInt(10) + 1;
    }

    @Override
    public Integer call() throws Exception {
        TimeUnit.SECONDS.sleep(sleepTime);
        log.info("Sleep time is " + sleepTime + " seconds");
        return sleepTime;
    }
}
