package com.rybak.model.task7_2;

import java.util.concurrent.LinkedBlockingQueue;

public class Sender implements Runnable {

    private LinkedBlockingQueue<Character> queue;

    public Sender() {
        queue = new LinkedBlockingQueue<>();
    }

    @Override
    public void run() {
        try {
            String string = "Info from sender";
            char[] characters = string.toCharArray();
            for (int i = 0; i < characters.length; i++) {
                queue.put(characters[i]);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public LinkedBlockingQueue<Character> getQueue() {
        return queue;
    }
}
