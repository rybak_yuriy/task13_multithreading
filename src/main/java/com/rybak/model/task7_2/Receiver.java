package com.rybak.model.task7_2;

import java.util.concurrent.LinkedBlockingQueue;

public class Receiver implements Runnable {

    private LinkedBlockingQueue<Character> queue;

    public Receiver(LinkedBlockingQueue<Character> queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        while (!queue.isEmpty()) {
            Character character = queue.poll();
            if (character != null) {
                System.out.print(character);
            }
        }
        System.out.println();
    }

}
