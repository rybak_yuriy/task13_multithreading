package com.rybak.model.task6_1;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SynchronizeOnSameObject {

    private static Logger log = LogManager.getLogger(SynchronizeOnSameObject.class);
    private final Object sync = new Object();

    public void runFirstMethod() {
        synchronized (sync) {
            for (int i = 0; i < 10; i++) {
                log.info("SynchronizeOnSameObject.Message form first method");
            }
        }

    }

    public void runSecondMethod() {
        synchronized (sync) {
            for (int i = 0; i < 10; i++) {
                log.info("SynchronizeOnSameObject.Message form second method");
            }
        }

    }

    public void runThirdMethod() {
        synchronized (sync) {
            for (int i = 0; i < 10; i++) {
                log.info("SynchronizeOnSameObject.Message form third method");
            }
        }

    }

}
