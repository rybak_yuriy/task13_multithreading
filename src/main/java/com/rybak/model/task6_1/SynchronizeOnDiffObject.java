package com.rybak.model.task6_1;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SynchronizeOnDiffObject {

    private static Logger log = LogManager.getLogger(SynchronizeOnDiffObject.class);
    private final Object syncFirst = new Object();
    private final Object syncSecond = new Object();
    private final Object syncThird = new Object();

    public void runFirstMethod() {
        synchronized (syncFirst) {
            for (int i = 0; i < 10; i++) {
                log.info("SynchronizeOnDiffObject.Message form first method");
            }
        }

    }

    public void runSecondMethod() {
        synchronized (syncSecond) {
            for (int i = 0; i < 10; i++) {
                log.info("SynchronizeOnDiffObject.Message form second method");
            }
        }

    }

    public void runThirdMethod() {
        synchronized (syncThird) {
            for (int i = 0; i < 10; i++) {
                log.info("SynchronizeOnDiffObject.Message form third method");
            }
        }

    }
}
