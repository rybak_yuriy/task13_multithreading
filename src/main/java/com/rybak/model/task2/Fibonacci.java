package com.rybak.model.task2;

import com.rybak.controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;

public class Fibonacci implements Runnable {

    private static Logger log = LogManager.getLogger(Fibonacci.class);
    private int number;
    private int[] sequence;

    public Fibonacci(int number) {
        this.number = number;
        sequence = new int[number];
    }

    private int getFibonacciNumber(int n) {
        if (n <= 1) {
            return n;
        }
        return getFibonacciNumber(n - 1) + getFibonacciNumber(n - 2);
    }

    @Override
    public void run() {
        for (int i = 0; i < number; i++) {
            sequence[i] = getFibonacciNumber(i);
        }
    }

    public int[] getSequence() {
        return sequence;
    }

    @Override
    public String toString() {
        return "Fibonacci{" +
                "number=" + number +
                ", sequence=" + Arrays.toString(sequence) +
                '}';
    }
}
