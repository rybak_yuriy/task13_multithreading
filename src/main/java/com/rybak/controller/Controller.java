package com.rybak.controller;

import com.rybak.model.task1.PingPong;
import com.rybak.model.task2.Fibonacci;
import com.rybak.model.task3.FibonacciExecutor;
import com.rybak.model.task4.FibonacciCallable;
import com.rybak.model.task5.FifthTask;
import com.rybak.model.task6_1.SynchronizeOnDiffObject;
import com.rybak.model.task6_1.SynchronizeOnSameObject;
import com.rybak.model.task6_2.SynchronizeOnDiffObjectLock;
import com.rybak.model.task6_2.SynchronizeOnSameObjectLock;
import com.rybak.model.task7_1.Receiver;
import com.rybak.model.task7_1.Sender;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.PipedOutputStream;
import java.util.*;
import java.util.concurrent.*;

public class Controller {

    private static Logger log = LogManager.getLogger(Controller.class);
    Random random = new Random();
    Scanner scanner = new Scanner(System.in);
    PingPong pingPong;

    public Controller() {
        pingPong = new PingPong();
    }

    public void doFirstTask() {
        pingPong.show();
        try {
            TimeUnit.MILLISECONDS.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        pingPong.setFlag(false);
    }

    public void doSecondTask() {
        List<Fibonacci> fibonacciList = new ArrayList<>();
        List<Thread> threadList = new ArrayList<>();
        final int threads = 4;
        for (int i = 0; i < threads; i++) {
            fibonacciList.add(new Fibonacci(random.nextInt(10) + 2));
        }
        for (int i = 0; i < threads; i++) {
            threadList.add(new Thread(fibonacciList.get(i)));
            fibonacciList.get(i).run();
        }
        for (int i = 0; i < threads; i++) {
            log.info(threadList.get(i).getName());
            log.info(Arrays.toString(fibonacciList.get(i).getSequence()));
        }
    }

    public void doThirdTask() {
        FibonacciExecutor fibonacci1 = new FibonacciExecutor(random.nextInt(10) + 2);
        FibonacciExecutor fibonacci2 = new FibonacciExecutor(random.nextInt(10) + 4);
        FibonacciExecutor fibonacci3 = new FibonacciExecutor(random.nextInt(10) + 6);
        FibonacciExecutor fibonacci4 = new FibonacciExecutor(random.nextInt(10) + 6);
        FibonacciExecutor fibonacci5 = new FibonacciExecutor(random.nextInt(10) + 6);
        FibonacciExecutor fibonacci6 = new FibonacciExecutor(random.nextInt(10) + 6);

        ExecutorService fixedExecutor = Executors.newFixedThreadPool(3);
        log.info("FixedThreadPool starts");
        fixedExecutor.execute(fibonacci1);
        fixedExecutor.execute(fibonacci2);
        fixedExecutor.execute(fibonacci3);
        fixedExecutor.shutdown();
        log.info("FixedThreadPool ends");

        ExecutorService singleExecutor = Executors.newSingleThreadExecutor();
        log.info("SingleThreadExecutor starts");
        singleExecutor.execute(fibonacci4);
        singleExecutor.execute(fibonacci5);
        singleExecutor.execute(fibonacci6);
        singleExecutor.shutdown();
        log.info("SingleThreadExecutor ends");

        ExecutorService scheduledExecutor = new ScheduledThreadPoolExecutor(3);
        log.info("ScheduledThreadPoolExecutor starts");
        scheduledExecutor.execute(fibonacci1);
        scheduledExecutor.execute(fibonacci2);
        scheduledExecutor.execute(fibonacci3);
        scheduledExecutor.shutdown();
        log.info("ScheduledThreadPoolExecutor ends");
    }

    public void doFourthTask() throws ExecutionException, InterruptedException {
        ExecutorService fixedExecutor = Executors.newFixedThreadPool(3);
        for (int i = 0; i < 10; i++) {
            Future<Integer> future = fixedExecutor.submit(
                    new FibonacciCallable(random.nextInt(10) + 2));
            log.info(future.get());
        }
        Thread.yield();
        fixedExecutor.shutdown();
    }

    public void doFifthTask() {
        FifthTask fifthTask = new FifthTask();
        ScheduledExecutorService service = Executors.newScheduledThreadPool(2);
        log.info("Enter how many tasks you want to run:");
        int tasks = scanner.nextInt();

        for (int i = 0; i < tasks; i++) {
            service.submit(fifthTask);
        }

        service.shutdown();
    }

    public void doSixthTask() {

        SynchronizeOnSameObject onSameObject = new SynchronizeOnSameObject();
        Thread thread1 = new Thread(() -> onSameObject.runFirstMethod());
        thread1.start();
        Thread thread2 = new Thread(() -> onSameObject.runSecondMethod());
        thread2.start();
        Thread thread3 = new Thread(() -> onSameObject.runThirdMethod());
        thread3.start();

        SynchronizeOnDiffObject onDiffObject = new SynchronizeOnDiffObject();
        Thread threadDiff1 = new Thread(() -> onDiffObject.runFirstMethod());
        threadDiff1.start();
        Thread threadDiff2 = new Thread(() -> onDiffObject.runSecondMethod());
        threadDiff2.start();
        Thread threadDiff3 = new Thread(() -> onDiffObject.runThirdMethod());
        threadDiff3.start();

    }

    public void doSeventhTask() {
        Sender sender = new Sender();
        PipedOutputStream pipedOutputStream = sender.getPipedOutputStream();
        Receiver receiver = new Receiver(pipedOutputStream);

        Thread threadSender = new Thread(sender);
        Thread threadReceiver = new Thread(receiver);

        threadSender.start();
        threadReceiver.start();
        try {
            threadReceiver.join();
            threadSender.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }

    public void doSixthTask2(){
        SynchronizeOnSameObjectLock onSameObject = new SynchronizeOnSameObjectLock();
        Thread thread1 = new Thread(() -> onSameObject.runFirstMethod());
        thread1.start();
        Thread thread2 = new Thread(() -> onSameObject.runSecondMethod());
        thread2.start();
        Thread thread3 = new Thread(() -> onSameObject.runThirdMethod());
        thread3.start();

        SynchronizeOnDiffObjectLock onDiffObject = new SynchronizeOnDiffObjectLock();
        Thread threadDiff1 = new Thread(() -> onDiffObject.runFirstMethod());
        threadDiff1.start();
        Thread threadDiff2 = new Thread(() -> onDiffObject.runSecondMethod());
        threadDiff2.start();
        Thread threadDiff3 = new Thread(() -> onDiffObject.runThirdMethod());
        threadDiff3.start();
    }

    public void doSeventhTask2(){
        com.rybak.model.task7_2.Sender sender = new com.rybak.model.task7_2.Sender();
        LinkedBlockingQueue<Character> characterLinkedBlockingQueue = sender.getQueue();
        com.rybak.model.task7_2.Receiver receiver =
                new com.rybak.model.task7_2.Receiver(characterLinkedBlockingQueue);

        Thread threadSender = new Thread(sender);
        Thread threadReceiver = new Thread(receiver);

        threadSender.start();
        threadReceiver.start();
        try {
            threadReceiver.join();
            threadSender.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }

}
