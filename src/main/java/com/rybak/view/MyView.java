package com.rybak.view;

import com.rybak.controller.Controller;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;

public class MyView {

    Scanner scanner = new Scanner(System.in);
    Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> menuMethods;

    public MyView() {
        controller = new Controller();
        menu = new LinkedHashMap<>();
        menuMethods = new LinkedHashMap<>();

        menu.put("1", "1 - Task 1 - PingPong");
        menu.put("2", "2 - Task 2 - Fibonacci sequence with runnable");
        menu.put("3", "3 - Task 3 - Fibonacci sequence with executors");
        menu.put("4", "4 - Task 4 - Get sum of fibonacci sequence with callable");
        menu.put("5", "5 - Task 5 - Test ScheduledThreadPool");
        menu.put("6", "6 - Task 6 - Test synchronize on the objects");
        menu.put("7", "7 - Task 7 - Communication by pipe");
        menu.put("8", "8 - Task 6_2 - Test Lock objects");
        menu.put("9", "9 - Task 7_2 - Test BlockingQueue");
        menu.put("Q", "Q - Exit");

        menuMethods.put("1", this::doFirstTask);
        menuMethods.put("2", this::doSecondTask);
        menuMethods.put("3", this::doThirdTask);
        menuMethods.put("4", this::doFourthTask);
        menuMethods.put("5", this::doFifthTask);
        menuMethods.put("6", this::doSixthTask);
        menuMethods.put("7", this::doSeventhTask);
        menuMethods.put("8", this::doSixthTask2);
        menuMethods.put("9", this::doSevenTask2);
    }


    private void doFirstTask() {
        controller.doFirstTask();
    }

    private void doSecondTask() {
        controller.doSecondTask();
    }

    private void doThirdTask() {
        controller.doThirdTask();
    }

    private void doFourthTask() {
        try {
            controller.doFourthTask();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void doFifthTask() {
        controller.doFifthTask();
    }

    private void doSixthTask() {
        controller.doSixthTask();

    }

    private void doSeventhTask() {
        controller.doSeventhTask();
    }

    private void doSixthTask2() {
        controller.doSixthTask2();
    }

    private void doSevenTask2() {
        controller.doSeventhTask2();
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        menu.values().forEach(System.out::println);
    }

    public void start() {
        String keyMenu;
        do {
            outputMenu();
            System.out.print("Please, select menu point: ");
            keyMenu = scanner.nextLine().toUpperCase();
            try {
                menuMethods.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
        scanner.close();
        System.exit(0);
    }

}
